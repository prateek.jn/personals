###############################################################################################
### The perfect Varnish 4.x+ configuration for Joomla, WordPress & other CMS based websites ###
###############################################################################################

# Varnish Reference:
# See the VCL chapters in the Users Guide at https://www.varnish-cache.org/docs/
# and https://www.varnish-cache.org/trac/wiki/VCLExamples for more examples.

# Marker to tell the VCL compiler that this VCL has been adapted to the
# new 4.0 format.

# Multiple Domain Handling [Varnish under High-Availability].
vcl 4.0;

# Imports
import std;
import directors;


#Multiple-Backend Nodes based on multiple Domains..
backend node1_kpr {
    .host = "192.168.200.37";
    .port = "80";
}

backend node2_kpr {
    .host = "192.168.200.5";
    .port = "80";
}

backend node3_kpr {
    .host = "192.168.200.28";
    .port = "80";
}


backend node1_kc {
    .host = "192.168.200.22";
    .port = "80";
}

backend node2_kc {
    .host = "192.168.200.45";
    .port = "80";
}

sub vcl_init {

    #creating multiple cluster objects.
    new cluster = directors.round_robin();
    new cluster_kc = directors.round_robin();
	 
    #for 1st Domain, marking it as 'kpr_'.
    cluster.add_backend(node1_kpr);
    cluster.add_backend(node2_kpr);
    cluster.add_backend(node3_kpr);

    #for 2nd Domain, marking it as 'kc_'.	
    cluster_kc.add_backend(node1_kc);
    cluster_kc.add_backend(node2_kc);

}

acl purge {
        "localhost";
        "192.168.200.0"/24;
        "192.168.200.37";
		"192.168.200.22";
}


sub vcl_recv {

 # allow PURGE from localhost, 192.168.200.0/24
    if (req.method == "PURGE") {
            if (!client.ip ~ purge) {
                    return(synth(405,"Not allowed."));
            }
            return (purge);
    }

# Change the domain1 & domain2 accordingly
    if (req.http.host ~ "^(.*\.)?domain1\.com$") {
        #You will need the following line only if your backend has multiple virtual host names
        set req.http.host = "www.domain1.com";
        set req.backend_hint = cluster.backend();
        //return (lookup);
    }
    if (req.http.host ~ "^(.*\.)?domain2\.com$") {
        #You will need the following line only if your backend has multiple virtual host names
        set req.http.host = "www.domain2.com";
        set req.backend_hint = cluster_kc.backend();
        //return (lookup);
    }


    //set req.backend_hint = cluster.backend();

    # Forward client's IP to the backend
    if (req.restarts == 0) {
        if (req.http.X-Real-IP) {
            set req.http.X-Forwarded-For = req.http.X-Real-IP;
        } else if (req.http.X-Forwarded-For) {
            set req.http.X-Forwarded-For = req.http.X-Forwarded-For + ", " + client.ip;
        } else {
            set req.http.X-Forwarded-For = client.ip;
        }
    }

    # httpoxy
    unset req.http.proxy;

    # Normalize the query arguments
    set req.url = std.querysort(req.url);

    # Remove Querystring from URL
    set req.url = regsub(req.url, "\?.*$", "");

    # Non-RFC2616 or CONNECT which is weird.
    if (
        req.method != "GET" &&
        req.method != "HEAD" &&
        req.method != "PUT" &&
        req.method != "POST" &&
        req.method != "TRACE" &&
        req.method != "OPTIONS" &&
        req.method != "DELETE"
    ) {
        return (pipe);
    }

    # We only deal with GET and HEAD by default
    if (req.method != "GET" && req.method != "HEAD") {
        return (pass);
    }

    # Don't cache HTTP authorization/authentication pages and pages with certain headers or cookies
    if (
        req.http.Authorization ||
        req.http.Authenticate ||
        req.http.Cookie ~ "c23bd2a0047189e89aa9bea67adbc1f0"
    ) {
        return (pass);
    }

    # Exclude the following paths (e.g. backend admins, user pages or ad URLs that require tracking)
    # In Joomla specifically, you are advised to create specific entry points (URLs) for users to
    # interact with the site (either common user logins or even commenting), e.g. make a menu item
    # to point to a user login page (e.g. /login), including all related functionality such as
    # password reset, email reminder and so on.
        if(req.url ~ "^/administrator") {
        return (pass);
    }

    # Don't cache ajax requests
    if(req.http.X-Requested-With == "XMLHttpRequest" || req.url ~ "nocache") {
        return (pass);
    }

    # Check for the custom "X-Logged-In" header (used by K2 and other apps) to identify
    # if the visitor is a guest, then unset any cookie (including session cookies) provided
    # it's not a POST request.
    if(req.http.Cookie ~ "c23bd2a0047189e89aa9bea67adbc1f0" && req.method != "POST") {
        unset req.http.Cookie;
    }

    # Properly handle different encoding types
    if (req.http.Accept-Encoding) {
      if (req.url ~ "\.(jpg|jpeg|png|gif|gz|tgz|bz2|tbz|mp3|ogg|swf)$") {
        # No point in compressing these
        unset req.http.Accept-Encoding;
      } elseif (req.http.Accept-Encoding ~ "gzip") {
        set req.http.Accept-Encoding = "gzip";
      } elseif (req.http.Accept-Encoding ~ "deflate") {
        set req.http.Accept-Encoding = "deflate";
      } else {
        # unknown algorithm (aka crappy browser)
        unset req.http.Accept-Encoding;
      }
    }

    # Remove all cookies for static files & deliver directly
    if (req.url ~ "^[^?]*\.(7z|avi|bmp|bz2|css|csv|doc|docx|eot|flac|flv|gif|gz|ico|jpeg|jpg|js|less|mka|mkv|mov|mp3|mp4|mpeg|mpg|odt|ogg|ogm|opus|otf|pdf|png|ppt|pptx|rar|rtf|svg|svgz|swf|tar|tbz|tgz|ttf|txt|txz|wav|webm|webp|woff|woff2|xls|xlsx|xml|xz|zip)(\?.*)?$") {
        unset req.http.Cookie;
        return (hash);
    }

    return (hash);

}

sub vcl_backend_response {

    # Don't cache 50x responses
    if (
        beresp.status == 500 ||
        beresp.status == 502 ||
        beresp.status == 503 ||
        beresp.status == 504
    ) {
        return (abandon);
    }

    # Exclude the following paths (e.g. backend admins, user pages or ad URLs that require tracking)
    # In Joomla specifically, you are advised to create specific entry points (URLs) for users to
    # interact with the site (either common user logins or even commenting), e.g. make a menu item
    # to point to a user login page (e.g. /login), including all related functionality such as
    # password reset, email reminder and so on.
    if(bereq.url ~ "^/administrator") {
        set beresp.uncacheable = true;
        return (deliver);
    }

    # Don't cache HTTP authorization/authentication pages and pages with certain headers or cookies
    if (
        bereq.http.Authorization ||
        bereq.http.Authenticate ||
        bereq.http.Cookie ~ "c23bd2a0047189e89aa9bea67adbc1f0"
    ) {
        set beresp.uncacheable = true;
        return (deliver);
    }

    # Don't cache ajax requests
    if(beresp.http.X-Requested-With == "XMLHttpRequest" || bereq.url ~ "nocache") {
        set beresp.uncacheable = true;
        return (deliver);
    }

    # Don't cache backend response to posted requests
    if (bereq.method == "POST") {
        set beresp.uncacheable = true;
        return (deliver);
    }

    # Ok, we're cool & ready to cache things
    # so let's clean up some headers and cookies
    # to maximize caching.

    # Check for the custom "X-Logged-In" header to identify if the visitor is a guest,
    # then unset any cookie (including session cookies) provided it's not a POST request.
    if(bereq.method != "POST" && beresp.http.Cookie ~ "c23bd2a0047189e89aa9bea67adbc1f0") {
        unset beresp.http.Set-Cookie;
    }

    # Unset the "etag" header (suggested)
    unset beresp.http.etag;

    # Unset the "pragma" header
    unset beresp.http.Pragma;

    # Allow stale content, in case the backend goes down
    set beresp.grace = 6h;

    # This is how long Varnish will keep cached content
    set beresp.ttl = 5m;

    # Modify "expires" header - https://www.varnish-cache.org/trac/wiki/VCLExampleSetExpires
    #set beresp.http.Expires = "" + (now + beresp.ttl);

    if (bereq.url ~ "^[^?]*\.(7z|avi|bmp|bz2|css|csv|doc|docx|eot|flac|flv|gif|gz|ico|jpeg|jpg|js|less|mka|mkv|mov|mp3|mp4|mpeg|mpg|odt|ogg|ogm|opus|otf|pdf|png|ppt|pptx|rar|rtf|svg|svgz|swf|tar|tbz|tgz|ttf|txt|txz|wav|webm|webp|woff|woff2|xls|xlsx|xml|xz|zip)(\?.*)?$") {
        unset beresp.http.set-cookie;
        set beresp.do_stream = true;
    }

    # We have content to cache, but it's got no-cache or other Cache-Control values sent
    # So let's reset it to our main caching time (2m as used in this example configuration)
    # The additional parameters specified (stale-while-revalidate & stale-if-error) are used
    # by modern browsers to better control caching. Set there to twice & five times your main
    # cache time respectively.
    # This final setting will normalize CMSs like Joomla which set max-age=0 even when
    # Joomla's cache is enabled.
    #if (beresp.http.Cache-Control !~ "max-age" || beresp.http.Cache-Control ~ "max-age=0") {
    #    set beresp.http.Cache-Control = "public, max-age=120, stale-while-revalidate=240, stale-if-error=480";
    #}

    return (deliver);

}

sub vcl_deliver {

    # Send special headers that indicate the cache status of each web page
    if (obj.hits > 0) {
        set resp.http.X-Cache = "HIT";
        set resp.http.X-Cache-Hits = obj.hits;
    } else {
        set resp.http.X-Cache = "MISS";
    }

    set resp.http.Via ="Varnish";
    unset resp.http.X-Powered-By;
    unset resp.http.Age;

    return (deliver);

}
